<?php
function fib($n) {
    $a = 0;
    $b = 1;
    for ($i=2; $i<$n; $i++) {
    	$b = $a + $b;
    	$a = $b;
    }
    return $b;
}